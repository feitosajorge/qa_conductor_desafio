@frontend 
Feature: Cenário de bugs encontrados na seção da transação no frontend do desafio da Conductor

@bug @transacao @adicionar
Scenario: Inserir nova venda sem selecionar o cliente e/ou adicionar o valor de transação
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de adicionar transação 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda" 
	When clicar em "salvar" transação 
	Then o sistema deve mostrar a mensagem de erro Campo Obrigatório
	And finaliza o browser  
	
	
@bug @transacao @adicionar @pesquisar 
Scenario:
Ir em listar transações, selecionar o cliente específico e pesquisar os detalhes de sua transação
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Nadia Lima" , cpf "678.909.454-17" e saldo "90" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de adicionar transação 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda" 
	When Eu adicionar uma transação para o nome "Nadia Lima" com valor "18" 
	And clicar em "salvar" transação 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda=" 
	When Eu acessar a área de listar transações 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda"
	When Eu clicar em pesquisar a transação de "Nadia Lima" 
	Then A lista deve retornar os clientes com nome "Nadia Lima" 
	And finaliza o browser
	

@bug @transacao @adicionar 
Scenario:
Aadicionar uma transação com o valor maior que o Saldo Disponível de um cliente
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Eliane Lima" , cpf "694.101.202-33" e saldo "100" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de adicionar transação 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda" 
	When Eu adicionar uma transação para o nome "Eliane Lima" com valor "400" 
	And clicar em "salvar" transação 
	Then o browser não deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda=" 
	And finaliza o browser