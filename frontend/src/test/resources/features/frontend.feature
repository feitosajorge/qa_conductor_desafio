@frontend 
Feature: Testar funções gerais do frontend do desafio da Conductor 

@login 
Scenario: Fazer login do usuário admin com senha admin 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	And finaliza o browser 
	
@login 
Scenario: Fazer login do usuário administrador com senha administrador 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "administrador" e senha "administrador" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/login?error" 
	And finaliza o browser 
	
@logout 
Scenario: Fazer login e clicar no botão de logout 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu clicar no botão de sair 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/login" 
	And finaliza o browser 
	
@login 
Scenario: Fazer login e clicar no botão de início 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu clicar no botão de início 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	And finaliza o browser 