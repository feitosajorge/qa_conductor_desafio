@frontend 
Feature: Cenário de bugs encontrados na seção do cliente no frontend do desafio da Conductor 

@bug @cliente @adicionar 
Scenario: Adicionar um novo cliente sem preencher o campo Saldo Disponível
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Túlio" , cpf "851.641.095-74" e saldo "" 
	And clico em "salvar" cliente 
	Then o sistema deve mostrar a mensagem de erro Campo Obrigatório
	And finaliza o browser  
	

@bug @cliente @lista
Scenario: Pesquisar na lista de clientes sem preencher ou o campo Nome ou o campo Data Validade, ambos marcados como obrigatórios
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de listar clientes 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	When Eu clicar me Pesquisar sem preencher Nome e/ou Data de Validade
	Then o sistema deve mostrar a mensagem de erro Campo Obrigatório
	And finaliza o browser 
	

@bug @cliente @limpar
Scenario: Ir para a seção de adicionar cliente, preencher os dados do mesmo e clicar em limpar para limpar os dados do input
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Carlos" , cpf "060.675.174-26" e saldo "100" 
	And clico em "limpar" cliente 
	Then o browser não deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente="
	And finaliza o browser  
	

@bug @cliente @adicionar 
Scenario: Adicionar o mesmo cliente duas vezes com o mesmo nome e CPF não deve ser válido
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Niédja" , cpf "111.222.888-55" e saldo "60" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Niédja" , cpf "111.222.888-55" e saldo "60" 
	And clico em "salvar" cliente 
	Then o browser não deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente="
	And finaliza o browser 
	
	
@bug @cliente @lista @limpar
Scenario: Pesquisar na lista de clientes, capturar um resultado gerado e clicar na ação de excluir cliente deve excluir o mesmo
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Maria do Carmo" , cpf "999.888.777-66" e saldo "200" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de listar clientes 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	When Eu pesquisar no campo nome "Maria do Carmo" 
	Then A lista deve retornar os clientes com nome "Maria do Carmo" 
	When Eu clicar em deletar cliente
	Then o browser não deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/alterarCliente/codigoCliente="
	And finaliza o browser 