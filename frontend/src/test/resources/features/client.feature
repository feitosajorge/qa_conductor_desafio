@frontend 
Feature: Testar o frontend na área do cliente do desafio da Conductor 

@cliente 
Scenario: Fazer login e ir para a seção de incluir cliente 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	And finaliza o browser 
	
@cliente @adicionar 
Scenario: Adicionar um novo cliente no formulário e salvar 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Jorge" , cpf "060.675.174-26" e saldo "30" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	And finaliza o browser 
	
	
@cliente 
Scenario: Adicionar um novo cliente no formulário e cancelar 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Jorge" , cpf "060.675.174-26" e saldo "30" 
	And clico em "cancelar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	And finaliza o browser 
	
	
@cliente @lista 
Scenario: Fazer login e ir para a seção de listar clientes 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de listar clientes 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	And finaliza o browser 
	
	
@cliente @adicionar @lista @pesquisar 
Scenario: Adicionar um novo cliente no formulário e cancelar 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "George Lima" , cpf "010.235.454-23" e saldo "40" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de listar clientes 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	When Eu pesquisar no campo nome "George Lima" 
	Then A lista deve retornar os clientes com nome "George Lima" 
	And finaliza o browser 
	
	
@cliente @adicionar @remover @lista @pesquisar 
Scenario:
Adicionar um novo cliente, salvar, ir para a página de lista dos clientes, limpar o banco de dados dos clientes e fazer uma pesquisa na lista atrás dos dados do cliente não deverá retornar nada
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Marcos Lima" , cpf "112.222.333-44" e saldo "70" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de listar clientes 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	When limpar a base de dados de clientes 
	When Eu pesquisar no campo nome "Marcos Lima" 
	Then A lista deve retornar vazia 
	And finaliza o browser 
	
	
@cliente @adicionar @lista @pesquisar 
Scenario:
Adicionar um novo cliente, salvar, verificar a lista de clientes, pesquisar pelo o cliente, localizar e clicar na opção de detalhes do cliente 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Mário Lima" , cpf "010.235.454-23" e saldo "40" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de listar clientes 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	When Eu pesquisar no campo nome "Mário Lima" 
	Then A lista deve retornar os clientes com nome "Mário Lima" 
	When Eu clicar em verificar detalhes do cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	And finaliza o browser 
	
	
	
@cliente @adicionar @lista @pesquisar @atualizar 
Scenario:
Adicionar um novo cliente, salvar, verificar a lista de clientes, pesquisar pelo o cliente, encontrar e clicar na opção de detalhes do cliente e atualizar os dados do mesmo 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Sérgio Lima" , cpf "777.888.994-23" e saldo "110" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de listar clientes 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	When Eu pesquisar no campo nome "Sérgio Lima" 
	Then A lista deve retornar os clientes com nome "Sérgio Lima" 
	When Eu clicar em verificar detalhes do cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu clicar em "alterar" na área de visualisar cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/alterarCliente/codigoCliente=" 
	Then eu preencho os campos de incluir cliente com nome "Nelson Lima" , cpf "000.888.994-23" e saldo "220" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	And finaliza o browser
	
	
@cliente @adicionar @lista @pesquisar 
Scenario:
Adicionar um novo cliente, salvar, verificar a lista de clientes, pesquisar pelo o cliente, encontrar e clicar na opção de detalhes do cliente e clicar no botão cancelar na página de visualisar cliente
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Pablo Lima" , cpf "123.456.789-23" e saldo "123" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de listar clientes 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	When Eu pesquisar no campo nome "Pablo Lima" 
	Then A lista deve retornar os clientes com nome "Pablo Lima" 
	When Eu clicar em verificar detalhes do cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu clicar em "cancelar" na área de visualisar cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente" 
	And finaliza o browser