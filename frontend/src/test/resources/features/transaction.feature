@frontend 
Feature: Testar o frontend na área das transações do desafio da Conductor

	
@transacao 
Scenario: Fazer login e ir para a seção de adicionar transações 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de adicionar transação 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda" 
	And finaliza o browser 
	
	
@transacao @lista 
Scenario: Fazer login e ir para a seção de listar transações 
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de listar transações 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda" 
	And finaliza o browser 
	
	
@transacao @adicionar 
Scenario:
Adicionar uma transação válida e salvar
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Ítalo Lima" , cpf "772.882.003-99" e saldo "90" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de adicionar transação 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda" 
	When Eu adicionar uma transação para o nome "Ítalo Lima" com valor "11" 
	And clicar em "salvar" transação 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda=" 
	And finaliza o browser 
	
	
@transacao @adicionar @pesquisar 
Scenario:
Ir em listar transações, selecionar TODOS e visualizar os detalhes da transação do primeiro resultado da lista que aparecer
	Given Acessar a página de login 
	When Fizer o login com as credenciais usuário "admin" e senha "admin" 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/inicio" 
	When Eu acessar a área de incluir cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente" 
	Then eu preencho os campos de incluir cliente com nome "Max Lima" , cpf "002.002.003-66" e saldo "75" 
	And clico em "salvar" cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente=" 
	When Eu acessar a área de adicionar transação 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda" 
	When Eu adicionar uma transação para o nome "Max Lima" com valor "17" 
	And clicar em "salvar" transação 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda=" 
	When Eu acessar a área de listar transações 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda"
	When Eu clicar em pesquisar a transação de "TODOS" 
	Then A lista deve retornar os clientes com nome "Max Lima" 
	When Eu clicar em verificar detalhes do cliente 
	Then o browser deve direcionar para a página "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda="
	And finaliza o browser