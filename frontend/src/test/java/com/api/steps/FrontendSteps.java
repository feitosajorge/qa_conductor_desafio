package com.api.steps;

import static org.junit.Assert.assertTrue;

import com.frontend.Selenium;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Testes a serem executados no frontend na seção de login, logout e navegação no site da Conductor
 */
public class FrontendSteps {
	
	public static final Selenium selenium = new Selenium("/desafioqa/");
	
	/**
	 * Se ocorrer um bug ou algum cenário falhar, fechar o browser
	 * @param scenario o cenário de teste retornado
	 */
	@After
	public void afterMethod(Scenario scenario) {
		if (scenario.isFailed()) {
			selenium.closeBrowserDriver();
		}
	}
	
	@Given("^Acessar a página de login$")
	public void acessar_a_pagina_de_login() throws Throwable {
		selenium.accessHomePage();
	}

	@When("^Fizer o login com as credenciais usuário \"(.*?)\" e senha \"(.*?)\"$")
	public void fizer_o_login_com_as_credenciais(String arg1, String arg2) throws Throwable {
		selenium.doLogin(arg1, arg2);
	}

	@Then("^o browser deve direcionar para a página \"(.*?)\"$")
	public void o_browser_deve_direcionar_para_a_pagina(String arg) throws Throwable {
	    assertTrue(Selenium.getDriver().getCurrentUrl().contains(arg));
	}
	
	@Then("^o browser não deve direcionar para a página \"(.*?)\"$")
	public void o_browser_nao_deve_direcionar_para_a_pagina(String arg) throws Throwable {
	    assertTrue(!Selenium.getDriver().getCurrentUrl().contains(arg));
	}
	
	@And("^finaliza o browser$")
	public void finaliza_o_browser() throws Throwable {
		selenium.closeBrowserDriver();
	}
	
	@When("^Eu clicar no botão de sair$")
	public void eu_clicar_no_botao_de_sair() throws Throwable {
	    selenium.logoutClick();
	}
	
	@When("^Eu clicar no botão de início$")
	public void eu_clicar_no_botao_de_inicio() throws Throwable {
	    selenium.inicioClick();
	}
	
	@Then("^o sistema deve mostrar a mensagem de erro Campo Obrigatório$")
	public void o_sistema_deve_mostrar_a_mensagem_de_erro_Campo_Obrigatorio() throws Throwable {
		selenium.findInvalidMessage();
	}
	
}
