package com.api.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Testes a serem executados no frontend na seção de clientes no site da Conductor
 */
public class ClientSteps {
	
	@When("^Eu acessar a área de incluir cliente$")
	public void eu_acessar_a_area_de_incluir_cliente() throws Throwable {
		FrontendSteps.selenium.incluirClienteAccess();
	}
	
	@Then("^eu preencho os campos de incluir cliente com nome \"([^\"]*)\" , cpf \"([^\"]*)\" e saldo \"([^\"]*)\"$")
	public void eu_preencho_os_campos_de_incluir_cliente_com_e(String arg1, String arg2, String arg3) throws Throwable {
		FrontendSteps.selenium.fillClientData(arg1, arg2, arg3);
	}
	
	@Then("^clico em \"([^\"]*)\" cliente$")
	public void clico_em_cliente(String arg) throws Throwable {
		FrontendSteps.selenium.incluirClienteAction(arg);
	}
	
	@When("^Eu acessar a área de listar clientes$")
	public void eu_acessar_a_area_de_listar_clientes() throws Throwable {
		FrontendSteps.selenium.listarClientesAccess();
	}
	
	@When("^Eu pesquisar no campo nome \"([^\"]*)\"$")
	public void eu_pesquisar_no_campo_nome(String arg) throws Throwable {
		FrontendSteps.selenium.searchClient(arg);
	}

	@Then("^A lista deve retornar os clientes com nome \"([^\"]*)\"$")
	public void a_lista_deve_retornar_os_clientes_com_nome(String arg) throws Throwable {
		FrontendSteps.selenium.assertSearchClientInResultList(arg);
	}
	
	@When("^limpar a base de dados de clientes$")
	public void limpar_a_base_de_dados_de_clientes() throws Throwable {
		FrontendSteps.selenium.clearClientDatabase();
	}
	
	@Then("^A lista deve retornar vazia$")
	public void a_lista_deve_retornar_vazia() throws Throwable {
		FrontendSteps.selenium.assertClientListIsEmpty();
	}
	
	@When("^Eu clicar em verificar detalhes do cliente$")
	public void eu_clicar_em_verificar_detalhes_do_cliente() throws Throwable {
		FrontendSteps.selenium.verifiyClientDetailsInList();
	}
	
	@When("^Eu clicar em \"([^\"]*)\" na área de visualisar cliente$")
	public void eu_clicar_em_na_area_de_visualisar_cliente(String arg) throws Throwable {
		FrontendSteps.selenium.visualisarClienteAction(arg);
	}
	
	@When("^Eu clicar me Pesquisar sem preencher Nome e/ou Data de Validade$")
	public void eu_clicar_me_Pesquisar_sem_preencher_Nome_e_ou_Data_de_Validade() throws Throwable {
		FrontendSteps.selenium.searchClick();
		FrontendSteps.selenium.findInvalidMessage();
	}
	
	@When("^Eu clicar em deletar cliente$")
	public void Eu_clicar_em_deletar_cliente() throws Throwable {
		FrontendSteps.selenium.deleteClientInList();
	}

}
