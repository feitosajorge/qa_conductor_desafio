package com.api.steps;

import cucumber.api.java.en.When;

/**
 * Testes a serem executados no frontend na seção de transação no site da Conductor
 */
public class TransactionSteps {
	
	@When("^Eu acessar a área de adicionar transação$")
	public void eu_acessar_a_area_de_adicionar_transacao() throws Throwable {
		FrontendSteps.selenium.incluirTransacaoAccess();
	}

	@When("^Eu acessar a área de listar transações$")
	public void eu_acessar_a_area_de_listar_transacoes() throws Throwable {
		FrontendSteps.selenium.listarTransacoesAccess();
	}
	
	@When("^Eu adicionar uma transação para o nome \"([^\"]*)\" com valor \"([^\"]*)\"$")
	public void eu_adicionar_uma_transacao_para_com_valor(String arg1, String arg2) throws Throwable {
		FrontendSteps.selenium.selectClientTransaction(arg1, arg2);
	}

	@When("^clicar em \"([^\"]*)\" transação$")
	public void clicar_em_transacao(String arg) throws Throwable {
		FrontendSteps.selenium.incluirTransacaoAction(arg);
	}
	
	@When("^Eu clicar em pesquisar a transação de \"([^\"]*)\"$") 
	public void eu_clicar_em_pesquisar_a_transacao_de(String arg) throws Throwable {
		FrontendSteps.selenium.transactionSearch(arg);
	}

}
