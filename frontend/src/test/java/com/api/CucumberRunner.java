package com.api;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/features",
        plugin={"html:target/result/cucumber-html-report","json:target/result/cucumber.json", "pretty:target/result/cucumber-pretty.txt",
        "usage:target/result/cucumber-usage.json","junit:target/result/cucumber-results.xml"}
    )

public class CucumberRunner {
	
}