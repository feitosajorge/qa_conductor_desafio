package com.frontend.transaction;

/**
 * Interface com funções básicas para navegação na área de transações
 */
public interface TransactionFeature {
	
	/**
	 * Acessa a URL de incluir vendas /desafioqa/incluirVenda
	 */
	public void incluirTransacaoAccess();
	
	/**
	 * Acessa a URL de listar vendas /desafioqa/listarVenda
	 */
	public void listarTransacoesAccess();
	
	/**
	 * Escolhe qual ação irá tomar quanto a incluir uma transação, como por exemplo, se irá salvar ou cancelar a transação
	 * @param action
	 */
	public void incluirTransacaoAction(String action);
	
	/**
	 * Faz a busca da transação de um dado cliente
	 * @param client o cliente a ser pesquisado
	 */
	public void transactionSearch(String client);
	
	/**
	 * Na seção de incluir transação, escolhe o cliente e insere o valor da transação
	 * @param name o nome do cliente
	 * @param value o valor da transação
	 */
	public void selectClientTransaction(String name, String value);

}
