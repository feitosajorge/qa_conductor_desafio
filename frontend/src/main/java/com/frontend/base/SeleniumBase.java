package com.frontend.base;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.Scenario;
import cucumber.api.java.After;

/**
 * Classe base do Selenium contendo configurações e funções básicas de navegação
 * ao frontend
 */
public abstract class SeleniumBase {
	
	// Path do driver do gecko para executar o browser do Firefox
	private static final String DRIVER_PATH = "drivers/geckodriver-win-64.exe";
	
	// URL base do site da Conductor
	private static final String BASEURL = "http://provaqa.marketpay.com.br:9084";
	
	// Path da URL que irá completar a URL
	private String path;

	private static WebDriver driver = null;

	/**
	 * O Construtor da classe
	 * 
	 * @param path
	 *            O path do serviço da API
	 */
	public SeleniumBase(String path) {
		this.path = path;
	}
	
	/**
	 * Se ocorrer um bug ou algum cenário falhar, fechar o browser
	 * @param scenario o cenário de teste retornado
	 */
	@After
	public void afterMethod(Scenario scenario) {
		if (scenario.isFailed()) {
			this.closeBrowserDriver();
		}
	}

	/**
	 * Cria uma nova instância do Driver do Firefox, insere uma espera implícia,
	 * quer dizer que qualquer busca por elementos na página levará o tempo inserido
	 * na espera implícia antes de lançar qualquer exceção e acessa a página de
	 * login da Conductor
	 */
	public void accessHomePage() {
		System.setProperty("webdriver.gecko.driver", DRIVER_PATH);

		driver = new FirefoxDriver();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		driver.get(BASEURL + path);
	}

	/**
	 * Inserir o usuário no login com o dado texto
	 * 
	 * @param text
	 *            texto a ser escrito no campo do usuário
	 */
	public void insertUsername(String text) {
		driver.findElement(By.name("username")).sendKeys(text);
	}

	/**
	 * Faz o login no site
	 * @param username o usuário
	 * @param password a senha
	 */
	public void doLogin(String username, String password) {
		this.insertUsername(username);
		this.insertPassword(password);
		this.loginClick();
	}

	/**
	 * Insere a senha no campo do login
	 * @param text o texto a ser inserido no campo
	 */
	public void insertPassword(String text) {
		driver.findElement(By.name("password")).sendKeys(text);
	}

	/**
	 * Clica no campo de fazer login
	 */
	public void loginClick() {
		driver.findElement(By.tagName("button")).click();
	}

	/**
	 * Clica no campo de fazer logout
	 */
	public void logoutClick() {
		driver.findElement(By.cssSelector("input[value='SAIR']")).click();
	}

	/**
	 * Clica no campo de Início na home page
	 */
	public void inicioClick() {
		driver.findElement(By.cssSelector("a[href='/desafioqa/inicio']")).click();
	}

	/**
	 * Busca genérica pelo o botão pesquisar no site
	 */
	public void searchClick() {
		driver.findElement(By.cssSelector("input[value='Pesquisar']")).click();
	}

	/**
	 * Checa que a mensagem de Campo Obrigatório aparece na interface e valida
	 */
	public void findInvalidMessage() {
		boolean found = false;

		try {
			found = driver.findElement(By.xpath("//small[@data-bv-result='INVALID']")) != null;
		} catch (NoSuchElementException nsee) {
		}

		assertTrue(found);
	}

	/**
	 * Fecha o browser do Firefox
	 */
	public void closeBrowserDriver() {
		driver.close();
	}

	/**
	 * @return o driver do browser
	 */
	public static WebDriver getDriver() {
		return driver;
	}

	/**
	 * @return a baseurl
	 */
	public static String getBaseurl() {
		return BASEURL;
	}

}
