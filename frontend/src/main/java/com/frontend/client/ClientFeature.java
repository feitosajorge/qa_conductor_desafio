package com.frontend.client;

/**
 * Interface com funções básicas para navegação na área de clientes
 */
public interface ClientFeature {

	/**
	 * Acessa a URL de incluir cliente /desafioqa/incluirCliente
	 */
	public void incluirClienteAccess();

	/**
	 * Pesquisa por um cliente específico na área de listar clientes e clica em
	 * Pesquisar para retornar o resultado da busca
	 * 
	 * @param name
	 *            o nome do cliente a ser pesquisado no campo Nome
	 */
	public void searchClient(String name);

	/**
	 * Verifica se o resultado de busca na seção de listar clientes retorna um
	 * cliente específico
	 * 
	 * @param name
	 *            o nome do cliente que deve retornar no resultado de busca
	 */
	public void assertSearchClientInResultList(String name);

	/**
	 * Ao pesquisar por algum nome na seção de listar clientes, deve retornar vazio,
	 * ou seja, não há nenhum cliente cadastrado de fato
	 */
	public void assertClientListIsEmpty();

	/**
	 * Acessa a área de listar clientes /desafioqa/listarCliente
	 */
	public void listarClientesAccess();

	/**
	 * Na área de cadastrar clientes, decidir qual ação irá tomar, por exemplo, se
	 * irá salvar, limpar ou cancelar a ação
	 * 
	 * @param action
	 *            a ação a ser tomada
	 */
	public void incluirClienteAction(String action);

	/**
	 * Limpar a base de dados do cliente
	 */
	public void clearClientDatabase();

	/**
	 * Preencher os campos de incluir cliente
	 * 
	 * @param name
	 *            o nome do cliente
	 * @param cpf
	 *            do cliente
	 * @param balance
	 *            o saldo disponível
	 */
	public void fillClientData(String name, String cpf, String balance);

	/**
	 * No resultado de busca na seção de listar clientes, se aparecer algum
	 * resultado, clica na ação de verificar detalhes do cliente do primeiro cliente
	 * que aparecer
	 */
	public void verifiyClientDetailsInList();

	/**
	 * Na seção de visualizar detalhes do cliente escolhe a ação que quer tomar, por
	 * exemplo, se quer alterar os dados do mesmo ou cancelar a ação
	 * 
	 * @param action a ação a ser tomada
	 */
	public void visualisarClienteAction(String action);

	/**
	 * Clica na seção de alterar cliente
	 */
	public void viewClientAction();
	
	/**
	 * Clicar na ação deletar cliente na lista de clientes
	 */
	public void deleteClientInList();

}
