package com.frontend.reporting;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;
import net.masterthought.cucumber.Reportable;

/**
 * 
 * Somente execute esta classe após executar a classe <code>CucumberRuuner.java</code>
 * 
 * Gerará um relatório na pasta no caminho frontend\target\cucumber-html-reports com vários arquivos para visualização
 *
 */
public class CucumberReport {
	
	public static void main(String[] args) {
		File reportOutputDirectory = new File("target");
		List<String> jsonFiles = new ArrayList<String>();
		jsonFiles.add("." + File.separator + "target" + File.separator + "result" + File.separator + "cucumber.json");

		String buildNumber = "1";
		String projectName = "qa_conductor_desafio";
		boolean runWithJenkins = false;
		boolean parallelTesting = false;

		Configuration configuration = new Configuration(reportOutputDirectory, projectName);
		
		// configurações adicionais
		configuration.setRunWithJenkins(runWithJenkins);
		configuration.setBuildNumber(buildNumber);
		
		// metadados adicionais para apresentar na página inicial
		configuration.addClassifications("Analista de Teste", "Jorge Luís de Lima Feitosa");
		configuration.addClassifications("empresa", "Conductor");
		configuration.addClassifications("Platform", "Windows");
		configuration.addClassifications("Browser", "Firefox");
		configuration.addClassifications("Site testado", "http://provaqa.marketpay.com.br:9084/desafioqa/");

		ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
		Reportable result = reportBuilder.generateReports();
	}

}