package com.frontend;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import com.frontend.base.SeleniumBase;
import com.frontend.client.ClientFeature;
import com.frontend.transaction.TransactionFeature;

/**
 * Classe de navegação no site da Conductor contendo todas as funções
 * importantes
 */
public class Selenium extends SeleniumBase implements TransactionFeature, ClientFeature {

	public Selenium(String path) {
		super(path);
	}

	public void incluirClienteAccess() {
		getDriver().navigate().to(getBaseurl() + "/desafioqa/incluirCliente");
	}

	public void incluirTransacaoAccess() {
		getDriver().navigate().to(getBaseurl() + "/desafioqa/incluirVenda");
	}

	public void searchClient(String name) {
		getDriver().findElement(By.name("j_idt17")).sendKeys(name);
		this.searchClick();
	}

	public void assertSearchClientInResultList(String name) {
		assertTrue(getDriver()
				.findElement(By.xpath("//tr[@class='success']//td[contains(text(),'" + name + "')]")) != null);
	}

	public void assertClientListIsEmpty() {
		boolean isEmpty = false;

		try {
			isEmpty = getDriver().findElement(By.xpath("//tr[@class='success']")) == null;
		} catch (NoSuchElementException nsee) {
			isEmpty = true;
		}

		assertTrue(isEmpty);
	}

	public void listarClientesAccess() {
		getDriver().navigate().to(getBaseurl() + "/desafioqa/listarCliente");
	}

	public void listarTransacoesAccess() {
		getDriver().navigate().to(getBaseurl() + "/desafioqa/listarVenda");
	}

	public void incluirClienteAction(String action) {
		if (action.equalsIgnoreCase("salvar")) {
			getDriver().findElement(By.cssSelector("button[id='botaoSalvar']")).click();
		} else if (action.equalsIgnoreCase("limpar")) {
			getDriver().findElement(By.cssSelector("button[id='botaoLimpar']")).click();
		} else if (action.equalsIgnoreCase("cancelar")) {
			getDriver().findElement(By.cssSelector("a[class*='btn-danger']")).click();
		}
	}

	public void incluirTransacaoAction(String action) {
		if (action.equalsIgnoreCase("salvar")) {
			getDriver().findElement(By.cssSelector("button[id='botaoSalvar']")).click();
		} else if (action.equalsIgnoreCase("cancelar")) {
			getDriver().findElement(By.cssSelector("a[class*='btn-danger']")).click();
		}
	}

	public void selectClientTransaction(String name, String value) {
		getDriver().findElement(By.xpath("//select[@id='cliente']//option[contains(text(),'" + name + "')]")).click();
		getDriver().findElement(By.name("valorTransacao")).sendKeys(value);
	}

	public void clearClientDatabase() {
		getDriver().findElement(By.cssSelector("input[value='Limpar Base']")).click();
	}

	public void fillClientData(String name, String cpf, String balance) {
		getDriver().findElement(By.name("nome")).clear();
		getDriver().findElement(By.name("cpf")).clear();
		getDriver().findElement(By.name("saldoCliente")).clear();
		getDriver().findElement(By.name("nome")).sendKeys(name);
		getDriver().findElement(By.name("cpf")).sendKeys(cpf);
		getDriver().findElement(By.name("saldoCliente")).sendKeys(balance);
	}

	public void verifiyClientDetailsInList() {
		getDriver().findElement(By.xpath("//tbody/tr[@class='success']//span[contains(@class,'fa-search')][1]"))
				.click();
	}
	
	public void deleteClientInList() {
		getDriver().findElement(By.xpath("//tbody/tr[@class='success']//span[contains(@class,'fa-trash')][1]"))
				.click();
	}

	public void visualisarClienteAction(String action) {
		if (action.equalsIgnoreCase("alterar")) {
			getDriver().findElement(By.cssSelector("a[href*='/desafioqa/alterarCliente/codigoCliente']")).click();
		} else if (action.equalsIgnoreCase("cancelar")) {
			getDriver().findElement(By.cssSelector("a[class*='btn-danger']")).click();
		}
	}

	public void viewClientAction() {
		getDriver().findElement(By.cssSelector("a[href*='/desafioqa/alterarCliente/']")).click();
	}

	public void transactionSearch(String client) {
		getDriver().findElement(By.xpath("//select[@id='cliente']//option[contains(text(),'"+ client + "')]")).click();
		getDriver().findElement(By.cssSelector("input[value='Pesquisar']")).click();
	}

}