# Teste do frontend do website do desafio da Conductor

URL testada: http://provaqa.marketpay.com.br:9084/desafioqa/

### Resultados:
- Presente na pasta *documents*
- Relatório e cenários de teste em arquivos no formato PDF e HTML presentes em pastas e subpastas 
- Para melhor verificação dos relatórios, usar os arquivos do formato HTML como o ```overview-features.html``` presente na pasta ```cucumber-html-reports``` e ```index.html``` presente na pasta ```result/cucumber-html-report```

### Tecnologias:

- Java
- Cucumber
- Maven
- Selenium Web Driver
- Gecko Driver (Firefox)

## Passos iniciais:

- Instale o Firefox em sua máquina
- Instale o [Gecko Driver](https://github.com/mozilla/geckodriver/releases) que se adapta ao seu sistema operacional
	- Há drivers para o Windows já presentes na pasta ```frontend/drivers``` 
- Dê o clone no projeto 
- Configure na classe ```SeleniumBase.java``` o caminho do executável do driver do Gecko na variável ```DRIVER_PATH```
	- ```private static final String DRIVER_PATH = "drivers/geckodriver-win-64.exe"```
- Execute o ```mvn clean package``` na pasta ```frontend``` para instalar as dependências

### Como executar:

- Execute a classe ```CucumberRunner.java``` no pacote de testes ```com.api```
- Após isso, execute a classe ```CucumberReport.java``` no pacote main do ```com.api.reporting```
- Verifique os resultados gerados dentro da pasta ***result*** e ***cucumber-html-reports*** na pasta target