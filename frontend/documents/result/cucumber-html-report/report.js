$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("client.feature");
formatter.feature({
  "line": 2,
  "name": "Testar o frontend na área do cliente do desafio da Conductor",
  "description": "",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@frontend"
    }
  ]
});
formatter.scenario({
  "line": 5,
  "name": "Fazer login e ir para a seção de incluir cliente",
  "description": "",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor;fazer-login-e-ir-para-a-seção-de-incluir-cliente",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@cliente"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 9231979193,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1846544575,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 9616644,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 2361030980,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6727683,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1682090531,
  "status": "passed"
});
formatter.after({
  "duration": 93867,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Adicionar um novo cliente no formulário e salvar",
  "description": "",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor;adicionar-um-novo-cliente-no-formulário-e-salvar",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 13,
      "name": "@cliente"
    },
    {
      "line": 13,
      "name": "@adicionar"
    }
  ]
});
formatter.step({
  "line": 15,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 16,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "eu preencho os campos de incluir cliente com nome \"Jorge\" , cpf \"060.675.174-26\" e saldo \"30\"",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8149057770,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1789504764,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 7258883,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 2419562846,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 8691204,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jorge",
      "offset": 51
    },
    {
      "val": "060.675.174-26",
      "offset": 65
    },
    {
      "val": "30",
      "offset": 90
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 252546667,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2445438483,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5402882,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1467550066,
  "status": "passed"
});
formatter.after({
  "duration": 20480,
  "status": "passed"
});
formatter.scenario({
  "line": 27,
  "name": "Adicionar um novo cliente no formulário e cancelar",
  "description": "",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor;adicionar-um-novo-cliente-no-formulário-e-cancelar",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 26,
      "name": "@cliente"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "eu preencho os campos de incluir cliente com nome \"Jorge\" , cpf \"060.675.174-26\" e saldo \"30\"",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "clico em \"cancelar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 10019032754,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 2027542625,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 11254619,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1932005518,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5034242,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jorge",
      "offset": 51
    },
    {
      "val": "060.675.174-26",
      "offset": 65
    },
    {
      "val": "30",
      "offset": 90
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 247543146,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "cancelar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2382142883,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 10584751,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1588157264,
  "status": "passed"
});
formatter.after({
  "duration": 34560,
  "status": "passed"
});
formatter.scenario({
  "line": 40,
  "name": "Fazer login e ir para a seção de listar clientes",
  "description": "",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor;fazer-login-e-ir-para-a-seção-de-listar-clientes",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 39,
      "name": "@cliente"
    },
    {
      "line": 39,
      "name": "@lista"
    }
  ]
});
formatter.step({
  "line": 41,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 42,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 43,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "Eu acessar a área de listar clientes",
  "keyword": "When "
});
formatter.step({
  "line": 45,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 46,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 9967594226,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 2052048235,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 14625286,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_listar_clientes()"
});
formatter.result({
  "duration": 2193353896,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5431896,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1511034245,
  "status": "passed"
});
formatter.after({
  "duration": 20906,
  "status": "passed"
});
formatter.scenario({
  "line": 50,
  "name": "Adicionar um novo cliente no formulário e cancelar",
  "description": "",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor;adicionar-um-novo-cliente-no-formulário-e-cancelar",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 49,
      "name": "@cliente"
    },
    {
      "line": 49,
      "name": "@adicionar"
    },
    {
      "line": 49,
      "name": "@lista"
    },
    {
      "line": 49,
      "name": "@pesquisar"
    }
  ]
});
formatter.step({
  "line": 51,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 52,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 54,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 55,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 56,
  "name": "eu preencho os campos de incluir cliente com nome \"George Lima\" , cpf \"010.235.454-23\" e saldo \"40\"",
  "keyword": "Then "
});
formatter.step({
  "line": 57,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 58,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 59,
  "name": "Eu acessar a área de listar clientes",
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 61,
  "name": "Eu pesquisar no campo nome \"George Lima\"",
  "keyword": "When "
});
formatter.step({
  "line": 62,
  "name": "A lista deve retornar os clientes com nome \"George Lima\"",
  "keyword": "Then "
});
formatter.step({
  "line": 63,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 9044934846,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1858542447,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 3799042,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 2205766061,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6676910,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "George Lima",
      "offset": 51
    },
    {
      "val": "010.235.454-23",
      "offset": 71
    },
    {
      "val": "40",
      "offset": 96
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 315740721,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2156534681,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4836269,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_listar_clientes()"
});
formatter.result({
  "duration": 1720176520,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 9076910,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "George Lima",
      "offset": 28
    }
  ],
  "location": "ClientSteps.eu_pesquisar_no_campo_nome(String)"
});
formatter.result({
  "duration": 1833307662,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "George Lima",
      "offset": 44
    }
  ],
  "location": "ClientSteps.a_lista_deve_retornar_os_clientes_com_nome(String)"
});
formatter.result({
  "duration": 21713503,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1489619409,
  "status": "passed"
});
formatter.after({
  "duration": 23467,
  "status": "passed"
});
formatter.scenario({
  "line": 67,
  "name": "",
  "description": "Adicionar um novo cliente, salvar, ir para a página de lista dos clientes, limpar o banco de dados dos clientes e fazer uma pesquisa na lista atrás dos dados do cliente não deverá retornar nada",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 66,
      "name": "@cliente"
    },
    {
      "line": 66,
      "name": "@adicionar"
    },
    {
      "line": 66,
      "name": "@remover"
    },
    {
      "line": 66,
      "name": "@lista"
    },
    {
      "line": 66,
      "name": "@pesquisar"
    }
  ]
});
formatter.step({
  "line": 69,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 70,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 71,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 72,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 73,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 74,
  "name": "eu preencho os campos de incluir cliente com nome \"Marcos Lima\" , cpf \"112.222.333-44\" e saldo \"70\"",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 76,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 77,
  "name": "Eu acessar a área de listar clientes",
  "keyword": "When "
});
formatter.step({
  "line": 78,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 79,
  "name": "limpar a base de dados de clientes",
  "keyword": "When "
});
formatter.step({
  "line": 80,
  "name": "Eu pesquisar no campo nome \"Marcos Lima\"",
  "keyword": "When "
});
formatter.step({
  "line": 81,
  "name": "A lista deve retornar vazia",
  "keyword": "Then "
});
formatter.step({
  "line": 82,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8572374431,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 2010234031,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6603096,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1810599599,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6319363,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Marcos Lima",
      "offset": 51
    },
    {
      "val": "112.222.333-44",
      "offset": 71
    },
    {
      "val": "70",
      "offset": 96
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 300220288,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2554147224,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5207469,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_listar_clientes()"
});
formatter.result({
  "duration": 1565790962,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 13785606,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.limpar_a_base_de_dados_de_clientes()"
});
formatter.result({
  "duration": 1793870845,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Marcos Lima",
      "offset": 28
    }
  ],
  "location": "ClientSteps.eu_pesquisar_no_campo_nome(String)"
});
formatter.result({
  "duration": 1574143818,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.a_lista_deve_retornar_vazia()"
});
formatter.result({
  "duration": 30057774105,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1478396790,
  "status": "passed"
});
formatter.after({
  "duration": 20480,
  "status": "passed"
});
formatter.scenario({
  "line": 86,
  "name": "",
  "description": "Adicionar um novo cliente, salvar, verificar a lista de clientes, pesquisar pelo o cliente, localizar e clicar na opção de detalhes do cliente",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 85,
      "name": "@cliente"
    },
    {
      "line": 85,
      "name": "@adicionar"
    },
    {
      "line": 85,
      "name": "@lista"
    },
    {
      "line": 85,
      "name": "@pesquisar"
    }
  ]
});
formatter.step({
  "line": 88,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 89,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 90,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 91,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 92,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 93,
  "name": "eu preencho os campos de incluir cliente com nome \"Mário Lima\" , cpf \"010.235.454-23\" e saldo \"40\"",
  "keyword": "Then "
});
formatter.step({
  "line": 94,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 95,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 96,
  "name": "Eu acessar a área de listar clientes",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 98,
  "name": "Eu pesquisar no campo nome \"Mário Lima\"",
  "keyword": "When "
});
formatter.step({
  "line": 99,
  "name": "A lista deve retornar os clientes com nome \"Mário Lima\"",
  "keyword": "Then "
});
formatter.step({
  "line": 100,
  "name": "Eu clicar em verificar detalhes do cliente",
  "keyword": "When "
});
formatter.step({
  "line": 101,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 102,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8951374485,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1822643125,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4614828,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1661834949,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 10213551,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Mário Lima",
      "offset": 51
    },
    {
      "val": "010.235.454-23",
      "offset": 70
    },
    {
      "val": "40",
      "offset": 95
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 281918414,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 1950908993,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4269229,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_listar_clientes()"
});
formatter.result({
  "duration": 1516147847,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 12469339,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Mário Lima",
      "offset": 28
    }
  ],
  "location": "ClientSteps.eu_pesquisar_no_campo_nome(String)"
});
formatter.result({
  "duration": 1825448032,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Mário Lima",
      "offset": 44
    }
  ],
  "location": "ClientSteps.a_lista_deve_retornar_os_clientes_com_nome(String)"
});
formatter.result({
  "duration": 22057823,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_clicar_em_verificar_detalhes_do_cliente()"
});
formatter.result({
  "duration": 1501435521,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 10007897,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1669745353,
  "status": "passed"
});
formatter.after({
  "duration": 76374,
  "status": "passed"
});
formatter.scenario({
  "line": 107,
  "name": "",
  "description": "Adicionar um novo cliente, salvar, verificar a lista de clientes, pesquisar pelo o cliente, encontrar e clicar na opção de detalhes do cliente e atualizar os dados do mesmo",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 106,
      "name": "@cliente"
    },
    {
      "line": 106,
      "name": "@adicionar"
    },
    {
      "line": 106,
      "name": "@lista"
    },
    {
      "line": 106,
      "name": "@pesquisar"
    },
    {
      "line": 106,
      "name": "@atualizar"
    }
  ]
});
formatter.step({
  "line": 109,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 110,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 111,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 112,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 113,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 114,
  "name": "eu preencho os campos de incluir cliente com nome \"Sérgio Lima\" , cpf \"777.888.994-23\" e saldo \"110\"",
  "keyword": "Then "
});
formatter.step({
  "line": 115,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 116,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 117,
  "name": "Eu acessar a área de listar clientes",
  "keyword": "When "
});
formatter.step({
  "line": 118,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 119,
  "name": "Eu pesquisar no campo nome \"Sérgio Lima\"",
  "keyword": "When "
});
formatter.step({
  "line": 120,
  "name": "A lista deve retornar os clientes com nome \"Sérgio Lima\"",
  "keyword": "Then "
});
formatter.step({
  "line": 121,
  "name": "Eu clicar em verificar detalhes do cliente",
  "keyword": "When "
});
formatter.step({
  "line": 122,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 123,
  "name": "Eu clicar em \"alterar\" na área de visualisar cliente",
  "keyword": "When "
});
formatter.step({
  "line": 124,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/alterarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 125,
  "name": "eu preencho os campos de incluir cliente com nome \"Nelson Lima\" , cpf \"000.888.994-23\" e saldo \"220\"",
  "keyword": "Then "
});
formatter.step({
  "line": 126,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 127,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 128,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8543592339,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1812066053,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5364055,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 2230735458,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 12359685,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sérgio Lima",
      "offset": 51
    },
    {
      "val": "777.888.994-23",
      "offset": 71
    },
    {
      "val": "110",
      "offset": 96
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 288507856,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2746310718,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5141762,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_listar_clientes()"
});
formatter.result({
  "duration": 1553040449,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 7193603,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sérgio Lima",
      "offset": 28
    }
  ],
  "location": "ClientSteps.eu_pesquisar_no_campo_nome(String)"
});
formatter.result({
  "duration": 2404773293,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sérgio Lima",
      "offset": 44
    }
  ],
  "location": "ClientSteps.a_lista_deve_retornar_os_clientes_com_nome(String)"
});
formatter.result({
  "duration": 24128011,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_clicar_em_verificar_detalhes_do_cliente()"
});
formatter.result({
  "duration": 1548193514,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5254829,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "alterar",
      "offset": 14
    }
  ],
  "location": "ClientSteps.eu_clicar_em_na_area_de_visualisar_cliente(String)"
});
formatter.result({
  "duration": 1789120337,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/alterarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5208749,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nelson Lima",
      "offset": 51
    },
    {
      "val": "000.888.994-23",
      "offset": 71
    },
    {
      "val": "220",
      "offset": 96
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 477657377,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 1717449693,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 27591266,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1565414214,
  "status": "passed"
});
formatter.after({
  "duration": 21760,
  "status": "passed"
});
formatter.scenario({
  "line": 132,
  "name": "",
  "description": "Adicionar um novo cliente, salvar, verificar a lista de clientes, pesquisar pelo o cliente, encontrar e clicar na opção de detalhes do cliente e clicar no botão cancelar na página de visualisar cliente",
  "id": "testar-o-frontend-na-área-do-cliente-do-desafio-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 131,
      "name": "@cliente"
    },
    {
      "line": 131,
      "name": "@adicionar"
    },
    {
      "line": 131,
      "name": "@lista"
    },
    {
      "line": 131,
      "name": "@pesquisar"
    }
  ]
});
formatter.step({
  "line": 134,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 135,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 136,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 137,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 138,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 139,
  "name": "eu preencho os campos de incluir cliente com nome \"Pablo Lima\" , cpf \"123.456.789-23\" e saldo \"123\"",
  "keyword": "Then "
});
formatter.step({
  "line": 140,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 141,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 142,
  "name": "Eu acessar a área de listar clientes",
  "keyword": "When "
});
formatter.step({
  "line": 143,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 144,
  "name": "Eu pesquisar no campo nome \"Pablo Lima\"",
  "keyword": "When "
});
formatter.step({
  "line": 145,
  "name": "A lista deve retornar os clientes com nome \"Pablo Lima\"",
  "keyword": "Then "
});
formatter.step({
  "line": 146,
  "name": "Eu clicar em verificar detalhes do cliente",
  "keyword": "When "
});
formatter.step({
  "line": 147,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 148,
  "name": "Eu clicar em \"cancelar\" na área de visualisar cliente",
  "keyword": "When "
});
formatter.step({
  "line": 149,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 150,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8540973884,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1810159279,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5633282,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1698745898,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 3744002,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Pablo Lima",
      "offset": 51
    },
    {
      "val": "123.456.789-23",
      "offset": 70
    },
    {
      "val": "123",
      "offset": 95
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 318523016,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2191769255,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4887895,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_listar_clientes()"
});
formatter.result({
  "duration": 1541084390,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 3942828,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Pablo Lima",
      "offset": 28
    }
  ],
  "location": "ClientSteps.eu_pesquisar_no_campo_nome(String)"
});
formatter.result({
  "duration": 1789357137,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Pablo Lima",
      "offset": 44
    }
  ],
  "location": "ClientSteps.a_lista_deve_retornar_os_clientes_com_nome(String)"
});
formatter.result({
  "duration": 34130788,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_clicar_em_verificar_detalhes_do_cliente()"
});
formatter.result({
  "duration": 1521307956,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6696109,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "cancelar",
      "offset": 14
    }
  ],
  "location": "ClientSteps.eu_clicar_em_na_area_de_visualisar_cliente(String)"
});
formatter.result({
  "duration": 1759703791,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6847150,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1589607505,
  "status": "passed"
});
formatter.after({
  "duration": 38400,
  "status": "passed"
});
formatter.uri("frontend.feature");
formatter.feature({
  "line": 2,
  "name": "Testar funções gerais do frontend do desafio da Conductor",
  "description": "",
  "id": "testar-funções-gerais-do-frontend-do-desafio-da-conductor",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@frontend"
    }
  ]
});
formatter.scenario({
  "line": 5,
  "name": "Fazer login do usuário admin com senha admin",
  "description": "",
  "id": "testar-funções-gerais-do-frontend-do-desafio-da-conductor;fazer-login-do-usuário-admin-com-senha-admin",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@login"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8409402628,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1715726386,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4023895,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1728923191,
  "status": "passed"
});
formatter.after({
  "duration": 49067,
  "status": "passed"
});
formatter.scenario({
  "line": 12,
  "name": "Fazer login do usuário administrador com senha administrador",
  "description": "",
  "id": "testar-funções-gerais-do-frontend-do-desafio-da-conductor;fazer-login-do-usuário-administrador-com-senha-administrador",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 11,
      "name": "@login"
    }
  ]
});
formatter.step({
  "line": 13,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "Fizer o login com as credenciais usuário \"administrador\" e senha \"administrador\"",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/login?error\"",
  "keyword": "Then "
});
formatter.step({
  "line": 16,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8522634890,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "administrador",
      "offset": 42
    },
    {
      "val": "administrador",
      "offset": 66
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 864320368,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/login?error",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 11887365,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1724832310,
  "status": "passed"
});
formatter.after({
  "duration": 41387,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Fazer login e clicar no botão de logout",
  "description": "",
  "id": "testar-funções-gerais-do-frontend-do-desafio-da-conductor;fazer-login-e-clicar-no-botão-de-logout",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 18,
      "name": "@logout"
    }
  ]
});
formatter.step({
  "line": 20,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "Eu clicar no botão de sair",
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/login\"",
  "keyword": "Then "
});
formatter.step({
  "line": 25,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 7993681917,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1768355741,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5093975,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.eu_clicar_no_botao_de_sair()"
});
formatter.result({
  "duration": 660301935,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/login",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4475735,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1775739637,
  "status": "passed"
});
formatter.after({
  "duration": 18347,
  "status": "passed"
});
formatter.scenario({
  "line": 28,
  "name": "Fazer login e clicar no botão de início",
  "description": "",
  "id": "testar-funções-gerais-do-frontend-do-desafio-da-conductor;fazer-login-e-clicar-no-botão-de-início",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 27,
      "name": "@login"
    }
  ]
});
formatter.step({
  "line": 29,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 30,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 31,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "Eu clicar no botão de início",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8753888749,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1762366619,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4317015,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.eu_clicar_no_botao_de_inicio()"
});
formatter.result({
  "duration": 1119238024,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 12620378,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1712797318,
  "status": "passed"
});
formatter.after({
  "duration": 25600,
  "status": "passed"
});
formatter.uri("transaction.feature");
formatter.feature({
  "line": 2,
  "name": "Testar o frontend na área das transações do desafio da Conductor",
  "description": "",
  "id": "testar-o-frontend-na-área-das-transações-do-desafio-da-conductor",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@frontend"
    }
  ]
});
formatter.scenario({
  "line": 6,
  "name": "Fazer login e ir para a seção de adicionar transações",
  "description": "",
  "id": "testar-o-frontend-na-área-das-transações-do-desafio-da-conductor;fazer-login-e-ir-para-a-seção-de-adicionar-transações",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 5,
      "name": "@transacao"
    }
  ]
});
formatter.step({
  "line": 7,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 8,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "Eu acessar a área de adicionar transação",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda\"",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8527078625,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1791084285,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4977922,
  "status": "passed"
});
formatter.match({
  "location": "TransactionSteps.eu_acessar_a_area_de_adicionar_transacao()"
});
formatter.result({
  "duration": 1125253174,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6196056,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1601982550,
  "status": "passed"
});
formatter.after({
  "duration": 20480,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Fazer login e ir para a seção de listar transações",
  "description": "",
  "id": "testar-o-frontend-na-área-das-transações-do-desafio-da-conductor;fazer-login-e-ir-para-a-seção-de-listar-transações",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 15,
      "name": "@transacao"
    },
    {
      "line": 15,
      "name": "@lista"
    }
  ]
});
formatter.step({
  "line": 17,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 18,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 20,
  "name": "Eu acessar a área de listar transações",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda\"",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8142498621,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1703786967,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4540162,
  "status": "passed"
});
formatter.match({
  "location": "TransactionSteps.eu_acessar_a_area_de_listar_transacoes()"
});
formatter.result({
  "duration": 1283945507,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 13222405,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1668636445,
  "status": "passed"
});
formatter.after({
  "duration": 17493,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "",
  "description": "Adicionar uma transação válida e salvar",
  "id": "testar-o-frontend-na-área-das-transações-do-desafio-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 25,
      "name": "@transacao"
    },
    {
      "line": 25,
      "name": "@adicionar"
    }
  ]
});
formatter.step({
  "line": 28,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 29,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "eu preencho os campos de incluir cliente com nome \"Ítalo Lima\" , cpf \"772.882.003-99\" e saldo \"90\"",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "Eu acessar a área de adicionar transação",
  "keyword": "When "
});
formatter.step({
  "line": 37,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda\"",
  "keyword": "Then "
});
formatter.step({
  "line": 38,
  "name": "Eu adicionar uma transação para o nome \"Ítalo Lima\" com valor \"11\"",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "clicar em \"salvar\" transação",
  "keyword": "And "
});
formatter.step({
  "line": 40,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 41,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8262112966,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1803907330,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 8588377,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 2304786477,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 10703792,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Ítalo Lima",
      "offset": 51
    },
    {
      "val": "772.882.003-99",
      "offset": 70
    },
    {
      "val": "90",
      "offset": 95
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 356312472,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2806783064,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 3226028,
  "status": "passed"
});
formatter.match({
  "location": "TransactionSteps.eu_acessar_a_area_de_adicionar_transacao()"
});
formatter.result({
  "duration": 1302114262,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 25871371,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Ítalo Lima",
      "offset": 40
    },
    {
      "val": "11",
      "offset": 63
    }
  ],
  "location": "TransactionSteps.eu_adicionar_uma_transacao_para_com_valor(String,String)"
});
formatter.result({
  "duration": 348393535,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 11
    }
  ],
  "location": "TransactionSteps.clicar_em_transacao(String)"
});
formatter.result({
  "duration": 1432070584,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6837336,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1960258969,
  "status": "passed"
});
formatter.after({
  "duration": 367360,
  "status": "passed"
});
formatter.scenario({
  "line": 45,
  "name": "",
  "description": "Ir em listar transações, selecionar TODOS e visualizar os detalhes da transação do primeiro resultado da lista que aparecer",
  "id": "testar-o-frontend-na-área-das-transações-do-desafio-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 44,
      "name": "@transacao"
    },
    {
      "line": 44,
      "name": "@adicionar"
    },
    {
      "line": 44,
      "name": "@pesquisar"
    }
  ]
});
formatter.step({
  "line": 47,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 48,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 49,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 50,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 51,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "eu preencho os campos de incluir cliente com nome \"Max Lima\" , cpf \"002.002.003-66\" e saldo \"75\"",
  "keyword": "Then "
});
formatter.step({
  "line": 53,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 55,
  "name": "Eu acessar a área de adicionar transação",
  "keyword": "When "
});
formatter.step({
  "line": 56,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda\"",
  "keyword": "Then "
});
formatter.step({
  "line": 57,
  "name": "Eu adicionar uma transação para o nome \"Max Lima\" com valor \"17\"",
  "keyword": "When "
});
formatter.step({
  "line": 58,
  "name": "clicar em \"salvar\" transação",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 60,
  "name": "Eu acessar a área de listar transações",
  "keyword": "When "
});
formatter.step({
  "line": 61,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda\"",
  "keyword": "Then "
});
formatter.step({
  "line": 62,
  "name": "Eu clicar em pesquisar a transação de \"TODOS\"",
  "keyword": "When "
});
formatter.step({
  "line": 63,
  "name": "A lista deve retornar os clientes com nome \"Max Lima\"",
  "keyword": "Then "
});
formatter.step({
  "line": 64,
  "name": "Eu clicar em verificar detalhes do cliente",
  "keyword": "When "
});
formatter.step({
  "line": 65,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 66,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8577822967,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1820282803,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4037548,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1476688417,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 18026248,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Max Lima",
      "offset": 51
    },
    {
      "val": "002.002.003-66",
      "offset": 68
    },
    {
      "val": "75",
      "offset": 93
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 281896227,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 1777053345,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 11886938,
  "status": "passed"
});
formatter.match({
  "location": "TransactionSteps.eu_acessar_a_area_de_adicionar_transacao()"
});
formatter.result({
  "duration": 1355746712,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5171629,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Max Lima",
      "offset": 40
    },
    {
      "val": "17",
      "offset": 61
    }
  ],
  "location": "TransactionSteps.eu_adicionar_uma_transacao_para_com_valor(String,String)"
});
formatter.result({
  "duration": 314898481,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 11
    }
  ],
  "location": "TransactionSteps.clicar_em_transacao(String)"
});
formatter.result({
  "duration": 1459639236,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5240749,
  "status": "passed"
});
formatter.match({
  "location": "TransactionSteps.eu_acessar_a_area_de_listar_transacoes()"
});
formatter.result({
  "duration": 1168149832,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 3851948,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TODOS",
      "offset": 39
    }
  ],
  "location": "TransactionSteps.eu_clicar_em_pesquisar_a_transacao_de(String)"
});
formatter.result({
  "duration": 1494124584,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Max Lima",
      "offset": 44
    }
  ],
  "location": "ClientSteps.a_lista_deve_retornar_os_clientes_com_nome(String)"
});
formatter.result({
  "duration": 13235633,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_clicar_em_verificar_detalhes_do_cliente()"
});
formatter.result({
  "duration": 1219119454,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 7591684,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "duration": 1668367218,
  "status": "passed"
});
formatter.after({
  "duration": 41813,
  "status": "passed"
});
formatter.uri("xbugs-client.feature");
formatter.feature({
  "line": 2,
  "name": "Cenário de bugs encontrados na seção do cliente no frontend do desafio da Conductor",
  "description": "",
  "id": "cenário-de-bugs-encontrados-na-seção-do-cliente-no-frontend-do-desafio-da-conductor",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@frontend"
    }
  ]
});
formatter.scenario({
  "line": 5,
  "name": "Adicionar um novo cliente sem preencher o campo Saldo Disponível",
  "description": "",
  "id": "cenário-de-bugs-encontrados-na-seção-do-cliente-no-frontend-do-desafio-da-conductor;adicionar-um-novo-cliente-sem-preencher-o-campo-saldo-disponível",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@bug"
    },
    {
      "line": 4,
      "name": "@cliente"
    },
    {
      "line": 4,
      "name": "@adicionar"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "eu preencho os campos de incluir cliente com nome \"Túlio\" , cpf \"851.641.095-74\" e saldo \"\"",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "o sistema deve mostrar a mensagem de erro Campo Obrigatório",
  "keyword": "Then "
});
formatter.step({
  "line": 14,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 11121053918,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 2586454011,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6855683,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 2289873617,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 3525122,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Túlio",
      "offset": 51
    },
    {
      "val": "851.641.095-74",
      "offset": 65
    },
    {
      "val": "",
      "offset": 90
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 295013246,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2854222871,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.o_sistema_deve_mostrar_a_mensagem_de_erro_Campo_Obrigatorio()"
});
formatter.result({
  "duration": 30016305927,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.frontend.base.SeleniumBase.findInvalidMessage(SeleniumBase.java:137)\r\n\tat com.api.steps.FrontendSteps.o_sistema_deve_mostrar_a_mensagem_de_erro_Campo_Obrigatorio(FrontendSteps.java:69)\r\n\tat ✽.Then o sistema deve mostrar a mensagem de erro Campo Obrigatório(xbugs-client.feature:13)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 1534944228,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Pesquisar na lista de clientes sem preencher ou o campo Nome ou o campo Data Validade, ambos marcados como obrigatórios",
  "description": "",
  "id": "cenário-de-bugs-encontrados-na-seção-do-cliente-no-frontend-do-desafio-da-conductor;pesquisar-na-lista-de-clientes-sem-preencher-ou-o-campo-nome-ou-o-campo-data-validade,-ambos-marcados-como-obrigatórios",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 17,
      "name": "@bug"
    },
    {
      "line": 17,
      "name": "@cliente"
    },
    {
      "line": 17,
      "name": "@lista"
    }
  ]
});
formatter.step({
  "line": 19,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "Eu acessar a área de listar clientes",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "Eu clicar me Pesquisar sem preencher Nome e/ou Data de Validade",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "o sistema deve mostrar a mensagem de erro Campo Obrigatório",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8804527276,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 2112105435,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5740802,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_listar_clientes()"
});
formatter.result({
  "duration": 1698705365,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 7149230,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_clicar_me_Pesquisar_sem_preencher_Nome_e_ou_Data_de_Validade()"
});
formatter.result({
  "duration": 31667587271,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.frontend.base.SeleniumBase.findInvalidMessage(SeleniumBase.java:137)\r\n\tat com.api.steps.ClientSteps.eu_clicar_me_Pesquisar_sem_preencher_Nome_e_ou_Data_de_Validade(ClientSteps.java:64)\r\n\tat ✽.When Eu clicar me Pesquisar sem preencher Nome e/ou Data de Validade(xbugs-client.feature:24)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "FrontendSteps.o_sistema_deve_mostrar_a_mensagem_de_erro_Campo_Obrigatorio()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 1590630225,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": "Ir para a seção de adicionar cliente, preencher os dados do mesmo e clicar em limpar para limpar os dados do input",
  "description": "",
  "id": "cenário-de-bugs-encontrados-na-seção-do-cliente-no-frontend-do-desafio-da-conductor;ir-para-a-seção-de-adicionar-cliente,-preencher-os-dados-do-mesmo-e-clicar-em-limpar-para-limpar-os-dados-do-input",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 29,
      "name": "@bug"
    },
    {
      "line": 29,
      "name": "@cliente"
    },
    {
      "line": 29,
      "name": "@limpar"
    }
  ]
});
formatter.step({
  "line": 31,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 32,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "eu preencho os campos de incluir cliente com nome \"Carlos\" , cpf \"060.675.174-26\" e saldo \"100\"",
  "keyword": "Then "
});
formatter.step({
  "line": 37,
  "name": "clico em \"limpar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "o browser não deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 39,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 9185611599,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1912194949,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5296215,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1673968928,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5646509,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Carlos",
      "offset": 51
    },
    {
      "val": "060.675.174-26",
      "offset": 66
    },
    {
      "val": "100",
      "offset": 91
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 251284588,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "limpar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2062568560,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 45
    }
  ],
  "location": "FrontendSteps.o_browser_nao_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 5410136,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.api.steps.FrontendSteps.o_browser_nao_deve_direcionar_para_a_pagina(FrontendSteps.java:49)\r\n\tat ✽.Then o browser não deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"(xbugs-client.feature:38)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 1534664761,
  "status": "passed"
});
formatter.scenario({
  "line": 43,
  "name": "Adicionar o mesmo cliente duas vezes com o mesmo nome e CPF não deve ser válido",
  "description": "",
  "id": "cenário-de-bugs-encontrados-na-seção-do-cliente-no-frontend-do-desafio-da-conductor;adicionar-o-mesmo-cliente-duas-vezes-com-o-mesmo-nome-e-cpf-não-deve-ser-válido",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 42,
      "name": "@bug"
    },
    {
      "line": 42,
      "name": "@cliente"
    },
    {
      "line": 42,
      "name": "@adicionar"
    }
  ]
});
formatter.step({
  "line": 44,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 45,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 48,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 49,
  "name": "eu preencho os campos de incluir cliente com nome \"Niédja\" , cpf \"111.222.888-55\" e saldo \"60\"",
  "keyword": "Then "
});
formatter.step({
  "line": 50,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 54,
  "name": "eu preencho os campos de incluir cliente com nome \"Niédja\" , cpf \"111.222.888-55\" e saldo \"60\"",
  "keyword": "Then "
});
formatter.step({
  "line": 55,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "o browser não deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 57,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8776265718,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1802548822,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4055042,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1771368436,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 10124378,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Niédja",
      "offset": 51
    },
    {
      "val": "111.222.888-55",
      "offset": 66
    },
    {
      "val": "60",
      "offset": 91
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 272119156,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 1907371054,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4114775,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1539867964,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 3925335,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Niédja",
      "offset": 51
    },
    {
      "val": "111.222.888-55",
      "offset": 66
    },
    {
      "val": "60",
      "offset": 91
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 302470102,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 1760210244,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 45
    }
  ],
  "location": "FrontendSteps.o_browser_nao_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 13459206,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.api.steps.FrontendSteps.o_browser_nao_deve_direcionar_para_a_pagina(FrontendSteps.java:49)\r\n\tat ✽.Then o browser não deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"(xbugs-client.feature:56)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 1591479292,
  "status": "passed"
});
formatter.scenario({
  "line": 61,
  "name": "Pesquisar na lista de clientes, capturar um resultado gerado e clicar na ação de excluir cliente deve excluir o mesmo",
  "description": "",
  "id": "cenário-de-bugs-encontrados-na-seção-do-cliente-no-frontend-do-desafio-da-conductor;pesquisar-na-lista-de-clientes,-capturar-um-resultado-gerado-e-clicar-na-ação-de-excluir-cliente-deve-excluir-o-mesmo",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 60,
      "name": "@bug"
    },
    {
      "line": 60,
      "name": "@cliente"
    },
    {
      "line": 60,
      "name": "@lista"
    },
    {
      "line": 60,
      "name": "@limpar"
    }
  ]
});
formatter.step({
  "line": 62,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 63,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 64,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 65,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 66,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 67,
  "name": "eu preencho os campos de incluir cliente com nome \"Maria do Carmo\" , cpf \"999.888.777-66\" e saldo \"200\"",
  "keyword": "Then "
});
formatter.step({
  "line": 68,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 69,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 70,
  "name": "Eu acessar a área de listar clientes",
  "keyword": "When "
});
formatter.step({
  "line": 71,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 72,
  "name": "Eu pesquisar no campo nome \"Maria do Carmo\"",
  "keyword": "When "
});
formatter.step({
  "line": 73,
  "name": "A lista deve retornar os clientes com nome \"Maria do Carmo\"",
  "keyword": "Then "
});
formatter.step({
  "line": 74,
  "name": "Eu clicar em deletar cliente",
  "keyword": "When "
});
formatter.step({
  "line": 75,
  "name": "o browser não deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/alterarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 76,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8686950693,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1859462340,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4491522,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1730393058,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 11293445,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Maria do Carmo",
      "offset": 51
    },
    {
      "val": "999.888.777-66",
      "offset": 74
    },
    {
      "val": "200",
      "offset": 99
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 330407821,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 1933762105,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4230401,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_listar_clientes()"
});
formatter.result({
  "duration": 1517308381,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 10186671,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Maria do Carmo",
      "offset": 28
    }
  ],
  "location": "ClientSteps.eu_pesquisar_no_campo_nome(String)"
});
formatter.result({
  "duration": 1796502953,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Maria do Carmo",
      "offset": 44
    }
  ],
  "location": "ClientSteps.a_lista_deve_retornar_os_clientes_com_nome(String)"
});
formatter.result({
  "duration": 35063055,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.Eu_clicar_em_deletar_cliente()"
});
formatter.result({
  "duration": 1550924608,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/alterarCliente/codigoCliente\u003d",
      "offset": 45
    }
  ],
  "location": "FrontendSteps.o_browser_nao_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6354350,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.api.steps.FrontendSteps.o_browser_nao_deve_direcionar_para_a_pagina(FrontendSteps.java:49)\r\n\tat ✽.Then o browser não deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/alterarCliente/codigoCliente\u003d\"(xbugs-client.feature:75)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 1583715662,
  "status": "passed"
});
formatter.uri("xbugs-transaction.feature");
formatter.feature({
  "line": 2,
  "name": "Cenário de bugs encontrados na seção da transação no frontend do desafio da Conductor",
  "description": "",
  "id": "cenário-de-bugs-encontrados-na-seção-da-transação-no-frontend-do-desafio-da-conductor",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@frontend"
    }
  ]
});
formatter.scenario({
  "line": 5,
  "name": "Inserir nova venda sem selecionar o cliente e/ou adicionar o valor de transação",
  "description": "",
  "id": "cenário-de-bugs-encontrados-na-seção-da-transação-no-frontend-do-desafio-da-conductor;inserir-nova-venda-sem-selecionar-o-cliente-e/ou-adicionar-o-valor-de-transação",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@bug"
    },
    {
      "line": 4,
      "name": "@transacao"
    },
    {
      "line": 4,
      "name": "@adicionar"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "Eu acessar a área de adicionar transação",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda\"",
  "keyword": "Then "
});
formatter.step({
  "line": 11,
  "name": "clicar em \"salvar\" transação",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "o sistema deve mostrar a mensagem de erro Campo Obrigatório",
  "keyword": "Then "
});
formatter.step({
  "line": 13,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8690188668,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1866984477,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 6271576,
  "status": "passed"
});
formatter.match({
  "location": "TransactionSteps.eu_acessar_a_area_de_adicionar_transacao()"
});
formatter.result({
  "duration": 1220152414,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 11953925,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 11
    }
  ],
  "location": "TransactionSteps.clicar_em_transacao(String)"
});
formatter.result({
  "duration": 256773657,
  "status": "passed"
});
formatter.match({
  "location": "FrontendSteps.o_sistema_deve_mostrar_a_mensagem_de_erro_Campo_Obrigatorio()"
});
formatter.result({
  "duration": 30016692913,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.frontend.base.SeleniumBase.findInvalidMessage(SeleniumBase.java:137)\r\n\tat com.api.steps.FrontendSteps.o_sistema_deve_mostrar_a_mensagem_de_erro_Campo_Obrigatorio(FrontendSteps.java:69)\r\n\tat ✽.Then o sistema deve mostrar a mensagem de erro Campo Obrigatório(xbugs-transaction.feature:12)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 1552735809,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "",
  "description": "Ir em listar transações, selecionar o cliente específico e pesquisar os detalhes de sua transação",
  "id": "cenário-de-bugs-encontrados-na-seção-da-transação-no-frontend-do-desafio-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 16,
      "name": "@bug"
    },
    {
      "line": 16,
      "name": "@transacao"
    },
    {
      "line": 16,
      "name": "@adicionar"
    },
    {
      "line": 16,
      "name": "@pesquisar"
    }
  ]
});
formatter.step({
  "line": 19,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 20,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 21,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 23,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 24,
  "name": "eu preencho os campos de incluir cliente com nome \"Nadia Lima\" , cpf \"678.909.454-17\" e saldo \"90\"",
  "keyword": "Then "
});
formatter.step({
  "line": 25,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 27,
  "name": "Eu acessar a área de adicionar transação",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda\"",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "Eu adicionar uma transação para o nome \"Nadia Lima\" com valor \"18\"",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "clicar em \"salvar\" transação",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 32,
  "name": "Eu acessar a área de listar transações",
  "keyword": "When "
});
formatter.step({
  "line": 33,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda\"",
  "keyword": "Then "
});
formatter.step({
  "line": 34,
  "name": "Eu clicar em pesquisar a transação de \"Nadia Lima\"",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "A lista deve retornar os clientes com nome \"Nadia Lima\"",
  "keyword": "Then "
});
formatter.step({
  "line": 36,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8968608413,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1867568157,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4998828,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 1646255636,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 15905287,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nadia Lima",
      "offset": 51
    },
    {
      "val": "678.909.454-17",
      "offset": 70
    },
    {
      "val": "90",
      "offset": 95
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 360591514,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2298825941,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4145495,
  "status": "passed"
});
formatter.match({
  "location": "TransactionSteps.eu_acessar_a_area_de_adicionar_transacao()"
});
formatter.result({
  "duration": 1349926976,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 3242241,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nadia Lima",
      "offset": 40
    },
    {
      "val": "18",
      "offset": 63
    }
  ],
  "location": "TransactionSteps.eu_adicionar_uma_transacao_para_com_valor(String,String)"
});
formatter.result({
  "duration": 308687492,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 11
    }
  ],
  "location": "TransactionSteps.clicar_em_transacao(String)"
});
formatter.result({
  "duration": 1451877312,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 12380592,
  "status": "passed"
});
formatter.match({
  "location": "TransactionSteps.eu_acessar_a_area_de_listar_transacoes()"
});
formatter.result({
  "duration": 1284475428,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/listarVenda",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 3679149,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nadia Lima",
      "offset": 39
    }
  ],
  "location": "TransactionSteps.eu_clicar_em_pesquisar_a_transacao_de(String)"
});
formatter.result({
  "duration": 1501690667,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nadia Lima",
      "offset": 44
    }
  ],
  "location": "ClientSteps.a_lista_deve_retornar_os_clientes_com_nome(String)"
});
formatter.result({
  "duration": 30016904967,
  "error_message": "org.openqa.selenium.NoSuchElementException: Unable to locate element: //tr[@class\u003d\u0027success\u0027]//td[contains(text(),\u0027Nadia Lima\u0027)]\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027LAPTOP-7HCRUNQL\u0027, ip: \u0027192.168.15.9\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_152\u0027\nDriver info: org.openqa.selenium.firefox.FirefoxDriver\nCapabilities {acceptInsecureCerts: true, browserName: firefox, browserVersion: 60.0.2, javascriptEnabled: true, moz:accessibilityChecks: false, moz:headless: false, moz:processID: 11460, moz:profile: C:\\Users\\Jorge\\AppData\\Loca..., moz:useNonSpecCompliantPointerOrigin: false, moz:webdriverClick: true, pageLoadStrategy: normal, platform: XP, platformName: XP, platformVersion: 10.0, rotatable: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}}\nSession ID: 749f1353-88b4-46b9-9013-a11e2052db23\n*** Element info: {Using\u003dxpath, value\u003d//tr[@class\u003d\u0027success\u0027]//td[contains(text(),\u0027Nadia Lima\u0027)]}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:548)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:322)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:424)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:314)\r\n\tat com.frontend.Selenium.assertSearchClientInResultList(Selenium.java:37)\r\n\tat com.api.steps.ClientSteps.a_lista_deve_retornar_os_clientes_com_nome(ClientSteps.java:38)\r\n\tat ✽.Then A lista deve retornar os clientes com nome \"Nadia Lima\"(xbugs-transaction.feature:35)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 1637693712,
  "status": "passed"
});
formatter.scenario({
  "line": 40,
  "name": "",
  "description": "Aadicionar uma transação com o valor maior que o Saldo Disponível de um cliente",
  "id": "cenário-de-bugs-encontrados-na-seção-da-transação-no-frontend-do-desafio-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 39,
      "name": "@bug"
    },
    {
      "line": 39,
      "name": "@transacao"
    },
    {
      "line": 39,
      "name": "@adicionar"
    }
  ]
});
formatter.step({
  "line": 42,
  "name": "Acessar a página de login",
  "keyword": "Given "
});
formatter.step({
  "line": 43,
  "name": "Fizer o login com as credenciais usuário \"admin\" e senha \"admin\"",
  "keyword": "When "
});
formatter.step({
  "line": 44,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/inicio\"",
  "keyword": "Then "
});
formatter.step({
  "line": 45,
  "name": "Eu acessar a área de incluir cliente",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente\"",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "eu preencho os campos de incluir cliente com nome \"Eliane Lima\" , cpf \"694.101.202-33\" e saldo \"100\"",
  "keyword": "Then "
});
formatter.step({
  "line": 48,
  "name": "clico em \"salvar\" cliente",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 50,
  "name": "Eu acessar a área de adicionar transação",
  "keyword": "When "
});
formatter.step({
  "line": 51,
  "name": "o browser deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda\"",
  "keyword": "Then "
});
formatter.step({
  "line": 52,
  "name": "Eu adicionar uma transação para o nome \"Eliane Lima\" com valor \"400\"",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "clicar em \"salvar\" transação",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "o browser não deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d\"",
  "keyword": "Then "
});
formatter.step({
  "line": 55,
  "name": "finaliza o browser",
  "keyword": "And "
});
formatter.match({
  "location": "FrontendSteps.acessar_a_pagina_de_login()"
});
formatter.result({
  "duration": 8522982196,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 42
    },
    {
      "val": "admin",
      "offset": 58
    }
  ],
  "location": "FrontendSteps.fizer_o_login_com_as_credenciais(String,String)"
});
formatter.result({
  "duration": 1777629772,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/inicio",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4171095,
  "status": "passed"
});
formatter.match({
  "location": "ClientSteps.eu_acessar_a_area_de_incluir_cliente()"
});
formatter.result({
  "duration": 2549590421,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirCliente",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 12483418,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Eliane Lima",
      "offset": 51
    },
    {
      "val": "694.101.202-33",
      "offset": 71
    },
    {
      "val": "100",
      "offset": 96
    }
  ],
  "location": "ClientSteps.eu_preencho_os_campos_de_incluir_cliente_com_e(String,String,String)"
});
formatter.result({
  "duration": 344375187,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 10
    }
  ],
  "location": "ClientSteps.clico_em_cliente(String)"
});
formatter.result({
  "duration": 2699926485,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarCliente/codigoCliente\u003d",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 4959575,
  "status": "passed"
});
formatter.match({
  "location": "TransactionSteps.eu_acessar_a_area_de_adicionar_transacao()"
});
formatter.result({
  "duration": 1330994488,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/incluirVenda",
      "offset": 41
    }
  ],
  "location": "FrontendSteps.o_browser_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 20328542,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Eliane Lima",
      "offset": 40
    },
    {
      "val": "400",
      "offset": 64
    }
  ],
  "location": "TransactionSteps.eu_adicionar_uma_transacao_para_com_valor(String,String)"
});
formatter.result({
  "duration": 327180939,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "salvar",
      "offset": 11
    }
  ],
  "location": "TransactionSteps.clicar_em_transacao(String)"
});
formatter.result({
  "duration": 1417573725,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d",
      "offset": 45
    }
  ],
  "location": "FrontendSteps.o_browser_nao_deve_direcionar_para_a_pagina(String)"
});
formatter.result({
  "duration": 11811845,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.assertTrue(Assert.java:41)\r\n\tat org.junit.Assert.assertTrue(Assert.java:52)\r\n\tat com.api.steps.FrontendSteps.o_browser_nao_deve_direcionar_para_a_pagina(FrontendSteps.java:49)\r\n\tat ✽.Then o browser não deve direcionar para a página \"http://provaqa.marketpay.com.br:9084/desafioqa/visualizarVenda/codigoVenda\u003d\"(xbugs-transaction.feature:54)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "FrontendSteps.finaliza_o_browser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 1648102250,
  "status": "passed"
});
});