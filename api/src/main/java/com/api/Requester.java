package com.api;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import gherkin.deps.com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Onde toda requisição a API é feita, filtrada e verificada
 */
public class Requester {

	// Base URL da API REST da Conductor
	private static final String BASEURL = "http://52.191.254.38";

	// Função de execução HTTP para a chamada na API
	private final CloseableHttpClient httpClient = HttpClients.createDefault();

	// Parâmetros adicionais para serem configurados no body da chamada
	public List<NameValuePair> params = new LinkedList<NameValuePair>();

	// Dados de resposta na chamada da API
	private HttpResponse httpResponse;

	// Conteúdo de resposta na chamada da API
	private String responseString;

	// Path que irá complementar a <code>BASEURL</code>
	private String path;

	// Header da resposta da requisição na API
	private Header header;

	/**
	 * O Construtor da classe
	 * 
	 * @param path
	 *            O path do serviço da API
	 */
	public Requester(String path) {
		this.path = path;
	}

	/**
	 * Função padrão para invocar a URL da API
	 * 
	 * @throws IOException
	 */
	public void callGetConductorApi() throws IOException {

		String paramString = URLEncodedUtils.format(params, "utf-8");

		HttpGet request = new HttpGet(BASEURL + path + "/?" + paramString);
		request.setHeader(header);
		httpResponse = httpClient.execute(request);
		responseString = convertResponseToString(httpResponse);
	}

	/**
	 * Verifica o status code da resposta da requisição HTTP
	 * 
	 * @param status_code
	 *            o código de resposta da response
	 */
	public void assertStatusCode(int status_code) {
		assert (httpResponse.getStatusLine().getStatusCode() == status_code);
	}

	/**
	 * Verifica se o formato do corpo da response é JSON ou não
	 * 
	 * @param status_code
	 *            o código de resposta da response
	 */
	public void assertIsJson(boolean isJson) {
		assert (toJsonList() != null) == isJson;
	}

	/**
	 * Checa se todo o JSON no array contém uma chave
	 * 
	 * @param key
	 *            a chave do JSON a ser verificada
	 * @param checkEmpty
	 *            ver se é para verificar se a chave contém conteúdo nulo ou não
	 */
	public void assertCheckKey(String key, boolean checkNull) {
		boolean isValid = true;

		for (Map<String, Object> json : toJsonList()) {
			if (!json.containsKey(key) || (checkNull && json.get(key) == null ? true : false)) {
				isValid = false;
				break;
			}
		}

		assert (isValid);
	}

	/**
	 * Converte String para ARRAY
	 * 
	 * @return os dados da API convertidos para ARRAY
	 */
	public List<Map<String, Object>> toJsonList() {
		List<Map<String, Object>> json = new ArrayList<Map<String, Object>>();

		try {
			Gson gson = new Gson();
			json = gson.fromJson(responseString, List.class);
		} catch (Exception e) {
			return null;
		}

		return json;
	}

	/**
	 * Verifica se as headers possui uma respectiva chave com um respectivo valor
	 * 
	 * @param key
	 *            a chave da header a ser analisada
	 * @param value
	 *            o valor da header a ser analisada
	 */
	public void assertCheckHeader(String key, String value) {

		boolean isValid = false;

		for (Header header : httpResponse.getAllHeaders()) {
			if (header.getName().equals(key) && header.getValue().equals(value)) {
				isValid = true;
				break;
			}
		}

		assert (isValid);
	}

	/**
	 * Converte o corpo da response para String
	 * 
	 * @param response
	 *            a resposta da request
	 * @return request body em formato String
	 * @throws IOException
	 */
	private String convertResponseToString(HttpResponse response) throws IOException {
		String responseString = "";
		if (response != null && response.getEntity() != null) {
			responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
		}
		return responseString;
	}
	
	/**
	 * 
	 * @return a BASEURL
	 */
	public static String getBASEURL() {
		return BASEURL;
	}
	
	/**
	 * 
	 * @return o httpClient
	 */
	public CloseableHttpClient getHttpClient() {
		return httpClient;
	}
	
	/**
	 * 
	 * @return os params
	 */
	public List<NameValuePair> getParams() {
		return params;
	}
	
	/**
	 * 
	 * @param params o params a armazenar
	 */
	public void setParams(List<NameValuePair> params) {
		this.params = params;
	}
	
	/**
	 * 
	 * @return o httpResponse
	 */
	public HttpResponse getHttpResponse() {
		return httpResponse;
	}
	
	/**
	 * 
	 * @param httpResponse a httpResponse a armazenar
	 */
	public void setHttpResponse(HttpResponse httpResponse) {
		this.httpResponse = httpResponse;
	}
	
	/**
	 * 
	 * @return a responseString
	 */
	public String getResponseString() {
		return responseString;
	}
	
	/**
	 * 
	 * @param responseString a responseString a armazenar
	 */
	public void setResponseString(String responseString) {
		this.responseString = responseString;
	}

	/**
	 * @return a header
	 */
	public Header getHeader() {
		return header;
	}

	/**
	 * @param header
	 *            a header a armazenar
	 */
	public void setHeader(Header header) {
		this.header = header;
	}

}