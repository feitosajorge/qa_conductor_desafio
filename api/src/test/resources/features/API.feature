@api 
Feature: Testar a API de produtos da Conductor 

@statuscode 
Scenario: Testar request na API com a Header Token: desafio 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o código de resposta deve ser 200 
	
@statuscode 
Scenario: Testar request na API com a Header Token: challenge 
	Given Setar a header da Request com Token: "challenge" 
	When Eu der a request para a API 
	Then o código de resposta deve ser 401 
	
@conteudo 
Scenario: Testar se o tipo de dados retornado é um JSON 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "ARRAY" 
	
@conteudo 
Scenario: Testar se o tipo de dados retornado é um NULL 
	Given Setar a header da Request com Token: "challenge" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "NULL" 
	
@conteudo 
Scenario: Testar se todo o JSON de array contém uma chave id 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "ARRAY" 
	And todo o json deve conter uma chave "id" 
	
@conteudo 
Scenario: Testar se todo o JSON de array contém uma chave nome 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "ARRAY" 
	And todo o json deve conter uma chave "nome" 
	
@conteudo 
Scenario: Testar se todo o JSON de array contém uma chave descricao 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "ARRAY" 
	And todo o json deve conter uma chave "descricao" 
	
@conteudo 
Scenario: Testar se todo o JSON de array contém uma chave valor_venda 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "ARRAY" 
	And todo o json deve conter uma chave "valor_venda" 
	
@conteudo 
Scenario: Testar se todo o JSON de array contém uma chave estoque 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "ARRAY" 
	And todo o json deve conter uma chave "estoque" 
	
@conteudo 
Scenario: Testar se todo o JSON de array contém uma chave endereco_foto 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "ARRAY" 
	And todo o json deve conter uma chave "endereco_foto" 
	
@conteudo 
Scenario: Testar se todo o JSON de array contém uma chave codigo_barras 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "ARRAY" 
	And todo o json deve conter uma chave "codigo_barras" 
	
@conteudo 
Scenario:
Testar se todo o JSON de array contém uma chave id não nula e não vazia 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then o formato da resposta dos dados deve ser "ARRAY" 
	And todo o json deve conter uma chave "id" não nula 
	
@header 
Scenario:
Testar se a header do conteúdo possui a header Content-Type com o valor application/json 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then a header "Content-Type" de resposta deve ser "application/json" 
	
@header 
Scenario:
Testar se a header do conteúdo possui a header Server com o valor Apache/2.4.18 (Ubuntu) 
	Given Setar a header da Request com Token: "desafio" 
	When Eu der a request para a API 
	Then a header "Server" de resposta deve ser "Apache/2.4.18 (Ubuntu)" 
	
  