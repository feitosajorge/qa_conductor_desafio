package com.api.steps;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import com.api.Requester;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Testes a serem executados na API de produtos da Conductor
 */
public class ApiSteps {
	
	Requester requester = new Requester("/desafio/produtos");
	
	@Given("^Setar a header da Request com Token: \"(.*?)\"$")
    public void setar_a_header_da_Request_com_Token(String arg) throws Throwable {
		Header header = new BasicHeader("Token", arg);
        requester.setHeader(header);
    }
	
	@When("^Eu der a request para a API$")
    public void eu_der_a_request_para_a_API() throws IOException {
    		requester.callGetConductorApi();
    }
	
	@Then("^o código de resposta deve ser (\\d+)$")
	public void o_codigo_de_resposta_deve_ser(int status_code) throws Throwable {
        requester.assertStatusCode(status_code);
    }
	
	@Then("^o formato da resposta dos dados deve ser \"(.*?)\"$")
	public void o_formato_da_resposta_dos_dados_deve_ser(String arg) throws Throwable {
        requester.assertIsJson(arg.equals("ARRAY") ? true : false);
    }
	
	@And("^todo o json deve conter uma chave \"(.*?)\"$")
	public void todo_o_json_deve_conter_uma_chave(String arg) throws Throwable {
        requester.assertCheckKey(arg, false);
    }
	
	@And("^todo o json deve conter uma chave \"([^\"]*)\" não nula$")
	public void todo_o_json_deve_conter_uma_chave_nao_nula(String arg) throws Throwable {
		requester.assertCheckKey(arg, true);
	}
	
	@Then("^a header \"([^\"]*)\" de resposta deve ser \"([^\"]*)\"$")
	public void a_header_de_resposta_deve_ser(String arg1, String arg2) throws Throwable {
		requester.assertCheckHeader(arg1, arg2);
	}
}
