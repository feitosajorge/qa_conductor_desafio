$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("API.feature");
formatter.feature({
  "line": 2,
  "name": "Testar a API de produtos da Conductor",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@api"
    }
  ]
});
formatter.scenario({
  "line": 5,
  "name": "Testar request na API com a Header Token: desafio",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-request-na-api-com-a-header-token:-desafio",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@statuscode"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 7,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "o código de resposta deve ser 200",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 925066634,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 521407369,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 30
    }
  ],
  "location": "ApiSteps.o_codigo_de_resposta_deve_ser(int)"
});
formatter.result({
  "duration": 1799254,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Testar request na API com a Header Token: challenge",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-request-na-api-com-a-header-token:-challenge",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 10,
      "name": "@statuscode"
    }
  ]
});
formatter.step({
  "line": 12,
  "name": "Setar a header da Request com Token: \"challenge\"",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "o código de resposta deve ser 401",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "challenge",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 3820375,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 398950571,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "401",
      "offset": 30
    }
  ],
  "location": "ApiSteps.o_codigo_de_resposta_deve_ser(int)"
});
formatter.result({
  "duration": 438186,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "Testar se o tipo de dados retornado é um JSON",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-se-o-tipo-de-dados-retornado-é-um-json",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 16,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 18,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 19,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "o formato da resposta dos dados deve ser \"ARRAY\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 10411098,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 414602417,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ARRAY",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 29274466,
  "status": "passed"
});
formatter.scenario({
  "line": 23,
  "name": "Testar se o tipo de dados retornado é um NULL",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-se-o-tipo-de-dados-retornado-é-um-null",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 22,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 24,
  "name": "Setar a header da Request com Token: \"challenge\"",
  "keyword": "Given "
});
formatter.step({
  "line": 25,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "o formato da resposta dos dados deve ser \"NULL\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "challenge",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 5452376,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 399637504,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "NULL",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 619520,
  "status": "passed"
});
formatter.scenario({
  "line": 29,
  "name": "Testar se todo o JSON de array contém uma chave id",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-se-todo-o-json-de-array-contém-uma-chave-id",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 28,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 30,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 31,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "o formato da resposta dos dados deve ser \"ARRAY\"",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "todo o json deve conter uma chave \"id\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 10979845,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 411066629,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ARRAY",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 6405123,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "id",
      "offset": 35
    }
  ],
  "location": "ApiSteps.todo_o_json_deve_conter_uma_chave(String)"
});
formatter.result({
  "duration": 6298456,
  "status": "passed"
});
formatter.scenario({
  "line": 36,
  "name": "Testar se todo o JSON de array contém uma chave nome",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-se-todo-o-json-de-array-contém-uma-chave-nome",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 35,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 37,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 38,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 39,
  "name": "o formato da resposta dos dados deve ser \"ARRAY\"",
  "keyword": "Then "
});
formatter.step({
  "line": 40,
  "name": "todo o json deve conter uma chave \"nome\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 5676803,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 403258199,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ARRAY",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 4715522,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "nome",
      "offset": 35
    }
  ],
  "location": "ApiSteps.todo_o_json_deve_conter_uma_chave(String)"
});
formatter.result({
  "duration": 7478190,
  "status": "passed"
});
formatter.scenario({
  "line": 43,
  "name": "Testar se todo o JSON de array contém uma chave descricao",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-se-todo-o-json-de-array-contém-uma-chave-descricao",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 42,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 44,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 45,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 46,
  "name": "o formato da resposta dos dados deve ser \"ARRAY\"",
  "keyword": "Then "
});
formatter.step({
  "line": 47,
  "name": "todo o json deve conter uma chave \"descricao\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 5519363,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 408434521,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ARRAY",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 7740590,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "descricao",
      "offset": 35
    }
  ],
  "location": "ApiSteps.todo_o_json_deve_conter_uma_chave(String)"
});
formatter.result({
  "duration": 3044695,
  "status": "passed"
});
formatter.scenario({
  "line": 50,
  "name": "Testar se todo o JSON de array contém uma chave valor_venda",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-se-todo-o-json-de-array-contém-uma-chave-valor-venda",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 49,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 51,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 52,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "o formato da resposta dos dados deve ser \"ARRAY\"",
  "keyword": "Then "
});
formatter.step({
  "line": 54,
  "name": "todo o json deve conter uma chave \"valor_venda\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 21704116,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 408844121,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ARRAY",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 2707201,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "valor_venda",
      "offset": 35
    }
  ],
  "location": "ApiSteps.todo_o_json_deve_conter_uma_chave(String)"
});
formatter.result({
  "duration": 2687574,
  "status": "passed"
});
formatter.scenario({
  "line": 57,
  "name": "Testar se todo o JSON de array contém uma chave estoque",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-se-todo-o-json-de-array-contém-uma-chave-estoque",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 56,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 58,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 59,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 60,
  "name": "o formato da resposta dos dados deve ser \"ARRAY\"",
  "keyword": "Then "
});
formatter.step({
  "line": 61,
  "name": "todo o json deve conter uma chave \"estoque\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 8502190,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 402293932,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ARRAY",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 1133654,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "estoque",
      "offset": 35
    }
  ],
  "location": "ApiSteps.todo_o_json_deve_conter_uma_chave(String)"
});
formatter.result({
  "duration": 1568854,
  "status": "passed"
});
formatter.scenario({
  "line": 64,
  "name": "Testar se todo o JSON de array contém uma chave endereco_foto",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-se-todo-o-json-de-array-contém-uma-chave-endereco-foto",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 63,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 65,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 66,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 67,
  "name": "o formato da resposta dos dados deve ser \"ARRAY\"",
  "keyword": "Then "
});
formatter.step({
  "line": 68,
  "name": "todo o json deve conter uma chave \"endereco_foto\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 3606188,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 396140969,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ARRAY",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 427093,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "endereco_foto",
      "offset": 35
    }
  ],
  "location": "ApiSteps.todo_o_json_deve_conter_uma_chave(String)"
});
formatter.result({
  "duration": 340480,
  "status": "passed"
});
formatter.scenario({
  "line": 71,
  "name": "Testar se todo o JSON de array contém uma chave codigo_barras",
  "description": "",
  "id": "testar-a-api-de-produtos-da-conductor;testar-se-todo-o-json-de-array-contém-uma-chave-codigo-barras",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 70,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 72,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 73,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 74,
  "name": "o formato da resposta dos dados deve ser \"ARRAY\"",
  "keyword": "Then "
});
formatter.step({
  "line": 75,
  "name": "todo o json deve conter uma chave \"codigo_barras\"",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 2045441,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 469803294,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ARRAY",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 2107307,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "codigo_barras",
      "offset": 35
    }
  ],
  "location": "ApiSteps.todo_o_json_deve_conter_uma_chave(String)"
});
formatter.result({
  "duration": 1487787,
  "status": "passed"
});
formatter.scenario({
  "line": 78,
  "name": "",
  "description": "Testar se todo o JSON de array contém uma chave id não nula e não vazia",
  "id": "testar-a-api-de-produtos-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 77,
      "name": "@conteudo"
    }
  ]
});
formatter.step({
  "line": 80,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 81,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 82,
  "name": "o formato da resposta dos dados deve ser \"ARRAY\"",
  "keyword": "Then "
});
formatter.step({
  "line": 83,
  "name": "todo o json deve conter uma chave \"id\" não nula",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 6021123,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 408372655,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "ARRAY",
      "offset": 42
    }
  ],
  "location": "ApiSteps.o_formato_da_resposta_dos_dados_deve_ser(String)"
});
formatter.result({
  "duration": 564480,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "id",
      "offset": 35
    }
  ],
  "location": "ApiSteps.todo_o_json_deve_conter_uma_chave_nao_nula(String)"
});
formatter.result({
  "duration": 592213,
  "status": "passed"
});
formatter.scenario({
  "line": 86,
  "name": "",
  "description": "Testar se a header do conteúdo possui a header Content-Type com o valor application/json",
  "id": "testar-a-api-de-produtos-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 85,
      "name": "@header"
    }
  ]
});
formatter.step({
  "line": 88,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 89,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 90,
  "name": "a header \"Content-Type\" de resposta deve ser \"application/json\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 2651307,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 404476333,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Content-Type",
      "offset": 10
    },
    {
      "val": "application/json",
      "offset": 46
    }
  ],
  "location": "ApiSteps.a_header_de_resposta_deve_ser(String,String)"
});
formatter.result({
  "duration": 506880,
  "status": "passed"
});
formatter.scenario({
  "line": 93,
  "name": "",
  "description": "Testar se a header do conteúdo possui a header Server com o valor Apache/2.4.18 (Ubuntu)",
  "id": "testar-a-api-de-produtos-da-conductor;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 92,
      "name": "@header"
    }
  ]
});
formatter.step({
  "line": 95,
  "name": "Setar a header da Request com Token: \"desafio\"",
  "keyword": "Given "
});
formatter.step({
  "line": 96,
  "name": "Eu der a request para a API",
  "keyword": "When "
});
formatter.step({
  "line": 97,
  "name": "a header \"Server\" de resposta deve ser \"Apache/2.4.18 (Ubuntu)\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "desafio",
      "offset": 38
    }
  ],
  "location": "ApiSteps.setar_a_header_da_Request_com_Token(String)"
});
formatter.result({
  "duration": 21952863,
  "status": "passed"
});
formatter.match({
  "location": "ApiSteps.eu_der_a_request_para_a_API()"
});
formatter.result({
  "duration": 406945027,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Server",
      "offset": 10
    },
    {
      "val": "Apache/2.4.18 (Ubuntu)",
      "offset": 40
    }
  ],
  "location": "ApiSteps.a_header_de_resposta_deve_ser(String,String)"
});
formatter.result({
  "duration": 267520,
  "status": "passed"
});
});