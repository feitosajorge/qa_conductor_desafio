# Teste da API da Conductor

URL testada: http://52.191.254.38/desafio/produtos

### Resultados:
- Presente na pasta *documents*
- Relatório e cenários de teste em arquivos no formato PDF e HTML presentes em pastas e subpastas 
- Para melhor verificação dos relatórios, usar os arquivos do formato HTML como o ```overview-features.html``` presente na pasta ```cucumber-html-reports``` e ```index.html``` presente na pasta ```result/cucumber-html-report```

### Tecnologias:

- Java
- Cucumber
- Maven

## Passos iniciais:

- Execute o ```mvn clean package``` na pasta ```api``` para instalar as dependências

### Como executar:

- Execute a classe ```CucumberRunner.java``` no pacote de testes ```com.api```
- Após isso, execute a classe ```CucumberReport.java``` no pacote main do ```com.api.reporting```
- Verifique os resultados gerados dentro da pasta ***result*** e ***cucumber-html-reports*** na pasta target